/**
 * Created by elf on 11/09/15.
 */

var api = require('./routes/api.js');

var routes = {

    setRoutes: function (app) {
        app.use( '/api', api() );
    }
};

module.exports = routes;