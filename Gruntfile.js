module.exports = function(grunt) {

    require('time-grunt-nowatch')(grunt);

    // Project configuration.
    grunt.initConfig({

        shell: {
            mongod: {
                command: 'mongod --dbpath backend/data/' //please replace with your command
            },
            options: {
                stdout: false,
                stderr: true,
                failOnError: true,
                async: true
            }
        },

        express: {
            web: {
                options: {
                    script: 'server.min.js'
                }
            }
        },

        open: {
            web: {
                url: 'http://localhost:3001'
            }
        },

        jshint: {
            grunt: ['Gruntfile.js'],
            dev: ['backend/**/*.js'],
            public: ['public/app/**/*.js']
        },

        less: {
          web: {
            options: {
              compress: true,
              sourceMap: true
            },
            files: {
              "public/styles.css": "public/less/main.less"
            }
          }
        },

        watch: {
            backend: {
                options: {
                    livereload: true,
                    spawn: false,
                    atBegin: false
                },
                files: [
                    'backend/**/*.js'
                ],
                tasks: [
                    'notify:changes',
                    'jshint:dev',
                    'node_optimize:dev',
                    'uglify:backend',
                    'clean:web',
                    'express:web',
                    'notify:complete'
                ]
            },
            frontend: {
                options: {
                    livereload: true
                },
                files: [
                    'public/index.html',
                    'public/app/**/*'
                ],
                tasks: [
                    'notify:changes',
                    'browserify',
                    'notify:complete'
                ]
            },
            less: {
                options: {
                    livereload: true
                },
                files: [
                    'public/less/**/*.less'
                ],
                tasks: [
                    'notify:changes',
                    'less',
                    'notify:complete'
                ]
            }
        },

        concat: {
            lib: {
                src: [
                    'public/lib/jquery.min.js',
                    'public/lib/underscore-min.js',
                    'public/lib/backbone-min.js',
                    'public/lib/backbone.babysitter.min.js',
                    'public/lib/backbone.wreqr.min.js',
                    'public/lib/backbone.marionette.min.js',
                    'public/lib/handlebars.min.js',
                    'public/lib/bootstrap.min.js'
                ],
                dest: 'public/infrastructure.js'
            }
        },

        browserify: {
            web: {
                files: {
                    'public/app.js': ['public/app/mainController.js']
                },
                options:
                {
                    transform: ['hbsfy'],
                    external: ['jquery', 'underscore', 'backbone', 'backbone.marionette']
                }
            },
            options: {
                debug: true
            }
        },

        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 10,
                title: 'SocialHub'
            }
        },

        notify: {
            complete: {
                options: {
                    title: 'SocialHub',
                    message: 'Project started'
                }
            },
            changes: {
                options: {
                    title: 'SocialHub',
                    message: 'Starting the project...'
                }
            }
        },

        node_optimize: {
            dev: {
                files: {
                    'server.min.js.bak': './backend/server.js'
                }
            }
        },

        uglify: {
            options: {
                compress: true,
                mangle: false,
                sourceMap: true
            },
            backend: {
                src: 'server.min.js.bak',
                dest: 'server.min.js'
            }
        },

        clean: {
            web: {
                src: ['server.min.js.bak']
            }
        }
    });

    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-open');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-node-optimize');
    grunt.loadNpmTasks('grunt-shell-spawn');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-concat');


    // Default task(s).
    grunt.registerTask('default', [
        'jshint:grunt',
        'shell:mongod',
        'node_optimize:dev',
        'uglify:backend',
        'clean:web',
        'express:web',
        'concat',
        'less',
        'browserify',
        'open:web',
        'watch'
    ]);
};