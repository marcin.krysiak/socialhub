require.config({
  "waitSeconds": 90,
  "baseUrl": "js",
  "enforceDefine": true,
  "paths": {
    "package": "package.json",
      "jquery": "libs/jquery/jquery",
      "jquery-ui": "libs/jquery-ui/js/jquery-ui-1.10.3.custom.min",
      "jquery-timepicker-addon": "libs/jquery-timepicker-addon",
      "underscore": "libs/underscore-amd/underscore",
      "backbone": "libs/backbone-amd/backbone",
      "bootstrap": "libs/bootstrap/js/bootstrap",
      "marionette": "libs/backbone.marionette/backbone.marionette",
      "backbone.wreqr": "libs/backbone.wreqr/backbone.wreqr",
      "backbone.babysitter": "libs/backbone.babysitter/backbone.babysitter",
      "moment": "libs/moment/min/moment-with-langs.min",
      "handlebars": "libs/handlebars/handlebars",
      "text": "libs/requirejs-text/text",
      "i18n": "libs/requirejs-i18n/i18n",
      "jquery-select2": "libs/select2/select2",
      "templates": "../tpl",
      //"facebook": "//connect.facebook.net/en_US/all", - is loaded async in channels view and modal
      "flexibleArea": "libs/flexibleArea/jquery.flexibleArea",
      "bootbox": "libs/bootbox/bootbox",
      "backbone-query-parameters": "libs/backbone-query-parameters/backbone.queryparams",
      "jquery-scrollTo": "libs/jquery.scrollTo/jquery.scrollTo.min",
      //"goog": "libs/requirejs/plugins/goog", - is loaded async in insights view
      "async": "libs/requirejs/plugins/async",
      "propertyParser": "libs/requirejs/plugins/propertyParser",
      "socket.io": "libs/socket.io-client/socket.io",
      "plupload": "libs/plupload/js/plupload.full.min",
      "plupload-de": "libs/plupload/js/i18n/de",
      "debug": "libs/debug/debug",
      "twitter-text": "libs/twitter-text/twitter-text",
      "selectize": "libs/selectize/selectize",
      "availableLanguages":  "config/availableLanguages"
    },
    "packages": [
      {
        "name": "codemirror",
        "location": "libs/CodeMirror",
        "main": "lib/codemirror"
      }
    ],
    //hardcode locale de
    //"locale": "de",
    "shim": {
        "plupload": {
            "exports": "plupload"
        },
        "plupload-de": {
            "deps": [
                "plupload"
            ],
            "exports": "plupload"
        },
        "handlebars": {
            "exports": "Handlebars"
        },
        "twitter-text": {
          "exports": "twttr.txt"
        },
        "bootstrap": {
            "deps": [
                "jquery"
            ],
            "exports": "jQuery"
        },
        "jquery-select2": {
            "deps": [
                "jquery"
            ],
            "exports": "Select2"
        },
        /*"facebook": {
            "exports": "FB"
        },*/
        "jquery-ui": {
            "deps": [
                "jquery"
            ],
            "exports": "jQuery"
        },
        "jquery-timepicker-addon": {
            "deps": [
                "jquery"
            ],
            "exports": "jQuery"
        },
        "bootbox": {
            "deps": [
                "bootstrap"
            ],
            "exports": "bootbox"
        },
        "flexibleArea": {
            "deps": [
                "jquery"
            ],
            "exports": "jQuery"
        },
        "jquery-scrollTo": {
            "deps": [
                "jquery"
            ],
            "exports": "jQuery"
        },
        "selectize": {
            "deps": [
                "jquery"
            ],
            "exports": "Selectize"
        }
    }
});

var locallocale = localStorage.locale;
if (locallocale) {
  require.config({
    "locale": locallocale
  });
}

define(["socket.io"], function (io) {
    // Initialize socket.io connection and add it to the io instance.
    io.socket = io(window.location.protocol + '//' + window.location.hostname);

    // Now initialize the frontend.
    require(['main.1.21.6']);
});
