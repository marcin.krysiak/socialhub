/**
 * Created by marcinkrysiak on 20/11/2015.
 */

var App = require('../../App.js'),
  AddonsView = require('./views/addons.js');

var addonsController = {

  addonsView: new AddonsView(),

  init: function () {
    App.addonsRegion.show(this.addonsView);
  }
};

module.exports = addonsController;