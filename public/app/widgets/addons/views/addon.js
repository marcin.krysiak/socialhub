
var View = Marionette.ItemView.extend({
  template: require('../templates/addon.hbs'),

  events: {
    'change .addon-checkbox input': 'toggleSelection'
  },

  toggleSelection: function(e) {
    var self = this;

    this.model.save({'checked': $(e.currentTarget).is(':checked') ? true : false})
      .done(function(data) {
        self.model.set(data);
        self.render();
      })
      .fail(function() {
        self.model.set({checked: self.model.previous('checked')} );
        self.render();
        alert('Couldn\'t save to the server. Please try later' );
      });
  },

  templateHelpers: function() {
    var self = this;

    return {
      disabled: function() {
        return self.model.get('type') === 'requested';
      },
      newOffer: function() {
        var time = self.model.get('createdTime'),
            today = new Date(),
            newOfferDate;

        newOfferDate = today;
        newOfferDate.setDate(today.getDate() - 30); //less than 30 day means the addon is "new"

        return newOfferDate - Date.parse(time) < 0;
      }
    }
  }
});

module.exports = View;