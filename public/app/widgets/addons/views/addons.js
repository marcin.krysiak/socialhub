var AddonView = require('./addon.js'),
    AddonsCollection = require('../models/addons.js');

var View = Marionette.CompositeView.extend({
  template: require('../templates/addonsBox.hbs'),
  childView: AddonView,
  childViewContainer : ".addons-container",
  collection: new AddonsCollection(),

  events: {
    'click .side-label': 'toggleHidden',
    'mouseover .side-label': 'toggleHidden'
  },

  initialize: function() {
    this.collection.fetch();
  },

  onShow: function() {
    var self = this,
        counter = 0;

    setTimeout(function() {
      self.$el.find('.side-label').addClass('animated bounceIn');
    }, 2000);
  },

  //show/hide addons panel
  toggleHidden: function(e) {
    var $widget = $(e.currentTarget).closest('.features-widget');

    $widget.find('.side-label').removeClass('animated bounceIn').addClass('off');

    $widget.toggleClass('collapsed');
    if ( !$widget.hasClass('collapsed') ) $widget.one('mouseleave', function(e) {
      $(this).addClass('collapsed');
    });
  }
});


module.exports = View;