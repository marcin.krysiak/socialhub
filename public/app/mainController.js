var App = new Backbone.Marionette.Application(),
  addonsController = require('./widgets/addons/controller.js');

App.on("start", function(){
  addonsController.init();
});

$(function() {
  App.start();
});