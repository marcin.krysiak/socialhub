
var App = new Backbone.Marionette.Application();

App.addRegions({
  addonsRegion: "#features-widget"
});


module.exports = App;